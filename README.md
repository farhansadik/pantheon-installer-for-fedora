# Pantheon Desktop for Fedora

By default pantheon desktop is not available on fedora repository. For pantheon desktop we've to use [terra repository](https://terra.fyralabs.com). Which is already included on installation script. 

## WARNING MESSAGE 
> Please take a look at the [issue](https://gitlab.com/farhansadik/pantheon-installer-for-fedora/-/issues/1) section. 

## DO NOT INSTALL...WAIT FOR THE NEXT FIX....
## STILL NO UPDATES....

### After entering user name and password - blank screen appears...

![a](images/1.PNG)


## Installation 
1. Clone Repository 
```bash
$ git clone https://gitlab.com/farhansadik/pantheon-installer-for-fedora.git
```
2. Run Installer <br>
```bash
$ sudo ./install
```

## Uninstall 
You can uninstall pantheon but executing
```bash 
$ sudo bash uninstall.bash
```


## Screenshot

![screen_1](images/desktop.png)

![screen_3](images/file.png)

![screen_2](images/sysinfo.png)

## Theme and Icon 
You'll have to download and install those manually. 

* [Theme - WhiteSur](https://github.com/vinceliuice/WhiteSur-gtk-theme)
* [Icon - WhiteSur](https://github.com/vinceliuice/WhiteSur-icon-theme)
* [Cursor - WhiteSur](https://www.gnome-look.org/p/1411743)
* [Wallpaper - Mono](https://github.com/witalihirsch/Mono-gtk-theme/tree/main/images/bg)

## Tweaks 

Theme - [WhiteSur](https://github.com/vinceliuice/WhiteSur-gtk-theme)

* Firefox Theme

Installation
```bash
$ git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git
$ ./tweaks.sh --firefox
```

Uninstall Firefox Theme
```bash
$ ./tweaks.sh --firefox -r
```

* GDM Theme 


## Sources
1. [Terra Repository](https://terra.fyralabs.com/)
2. [Pantheon Group](https://repology.org/projects/?search=pantheon&maintainer=&category=&inrepo=&notinrepo=&repos=&families=&repos_newest=&families_newest=) 
3. [Elementary Group](https://repology.org/projects/?search=elementary&maintainer=&category=&inrepo=&notinrepo=&repos=&families=&repos_newest=&families_newest=)

## Farhan Sadik
